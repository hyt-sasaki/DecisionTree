var DecisionTree = (function () {
    /* コンストラクタ */
    var DecisionTree = function (classNum, maxDepth) {
        this.classNum = classNum;
        if (maxDepth === undefined) {
            this.maxDepth = Number.MAX_VALUE;
        } else {
            this.maxDepth = maxDepth;
        }
    }

    var pt = DecisionTree.prototype;

    /* 学習 */
    pt.train = function (D) {
        this.root = new Node(D, this.classNum, 0, this.maxDepth);
        this.root.build();
    }

    pt.predict = function (x) {
        return this.root.predict(x);
    }

    return DecisionTree;
})();

var Node = (function () {
    var Node = function (D, classNum, depth, maxDepth) {
        this.D = D;
        this.classNum = classNum;
        this.depth = depth;
        this.maxDepth = maxDepth;
        this.children = [];
        this.calcImpureness();
    }

    var pt = Node.prototype;

    pt.build = function () {
        var maxDeltaI = 0;
        var maxTheta = -1;
        var bestChildren = [];

        for (var theta=0; theta < this.D[0].x.length; ++theta) {
            var LR = this.devideData(theta);
            var L = LR[0];
            var R = LR[1];
            var pL = L.length / (L.length + R.length);
            var pR = R.length / (L.length + R.length);
            var lNode = new Node(L, this.classNum, this.depth + 1, this.maxDepth);
            var rNode = new Node(R, this.classNum, this.depth + 1, this.maxDepth);

            var deltaI = this.impureness - (pL * lNode.impureness + pR * rNode.impureness);
            if (deltaI > maxDeltaI) {
                maxTheta = theta;
                maxDeltaI = deltaI;
                bestChildren[0] = lNode;
                bestChildren[1] = rNode;
            }
        }
        if (maxTheta !== -1 && this.depth < this.maxDepth) {
            this.children = bestChildren;
            this.theta = maxTheta;
            for (var i=0; i < this.children.length; ++i) {
                this.children[i].y = this.y + 1;
                this.children[i].build();
            }
        }
    }

    pt.calcImpureness = function () {
        var impureness = 1;
        var proportions = this.calcProportion();
        for (var i=0; i < proportions.length; ++i) {
            impureness -= Math.pow(proportions[i], 2);
        }
        this.impureness = impureness;
    }

    pt.calcProportion = function () {
        var dataNum = this.D.length;
        var proportion = [];
        for (var i=0; i < this.classNum; ++i) {
            proportion.push(0);
        }
        for (var i=0; i < dataNum; ++i) {
            proportion[this.D[i].y]++;
        }
        for (var i=0; i < this.classNum; ++i) {
            proportion[i] /= dataNum;
        }

        return proportion;
    }

    pt.devideData = function (theta) {
        var L = [];
        var R = [];
        for (var i=0; i < this.D.length; ++i) {
            if (this.D[i].x[theta] == false) {
                L.push(this.D[i]);
            } else {
                R.push(this.D[i]);
            }
        }

        return [L, R];
    }

    pt.predict = function (x) {
        if (this.children.length !== 0) {
            this.children[Number(x[this.theta])].predict(x);
        } else {
            return this.getMaxProportionClass();
        }
    }

    pt.getMaxProportionClass = function () {
        var proportions = this.calcProportion();
        var c = -1;
        var maxProportion = 0;
        for (var i=0; i < proportions.length; ++i) {
            if (proportions[i] > maxProportion) {
                c = i;
                maxProportion = proportions[i];
            }
        }
        return c;
    }

    return Node;
})();
