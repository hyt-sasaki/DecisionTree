var trueColor = '#ffff00';
var falseColor = '#00ffff';

$(function () {
    /* 訓練データの設定 */
    var Data = [];
    var ItemNames = ["spectacle", "astigmatism", "tear-prod"];
    var ClassNames = ["none", "soft", "hard"];

    (function () {
        var X = [
            [0, 0, 0],
            [0, 0, 1],
            [0, 1, 0],
            [0, 1, 1],
            [1, 0, 0],
            [1, 0, 1],
            [1, 1, 0],
            [1, 1, 1],
        ];
        var Y = [0, 1, 0, 2, 0, 1, 1, 1];
        for (var i=0; i < X.length; ++i) {
            Data.push(
                {x: X[i], y:Y[i]}
            );
        }
    })();

    /* DOM要素の作成 */
    makeTable(Data, ItemNames, ClassNames);
    makeClassNameInput(Data, ClassNames);

    /* 学習開始ボタンのハンドラ */
    $('#train').on('click', {D: Data, classNames:ClassNames, itemNames:ItemNames}, trainHandler);

    $('#addRow').on('click', {D: Data, classNames: ClassNames, table:$('#dataTable')}, addRow);
    $('#delRow').on('click', {D: Data, table:$('#dataTable')}, delRow);
    $('#addColumn').on('click', {D: Data, L: ItemNames}, addColumn);
    $('#delColumn').on('click', {D: Data, L: ItemNames}, delColumn);
});

function trHandler(event) {
    var x = event.data.x;
    var i = event.data.i;
    $(this).text(String(Boolean(!x[i])));
    $(this).css("background-color", (!x[i]) ? trueColor:falseColor);
    x[i] = Number(!x[i]);
}

function selectHandler(event) {
    event.data.d.y = $(this).val();
}

function trainHandler(event) {
    var classNames = event.data.classNames;
    var itemNames = event.data.itemNames;
    var classNum = classNames.length;
    var D = event.data.D;
    var maxDepth = Number($('#depth').val());
    var decisionTree = new DecisionTree(classNum, maxDepth);
    decisionTree.train(D);
    var canvas = window.document.getElementById("mainCanvas");
    var nd = new NodeDrawable(decisionTree.root, 0);
    nd.calcLayout();
    var rectWidth = calcNodeWidth(['不純度: 0.000e-00'].concat(itemNames).concat(classNames), canvas);
    var rectHeight = 40;
    calcCanvasSize(nd, rectWidth, rectHeight, canvas);
    clearCanvas(canvas);
    drawTree(nd, itemNames, classNames, canvas, rectWidth, rectHeight);
}

function calcNodeWidth(labels, canvas) {
    var ctx = canvas.getContext('2d');
    ctx.font = '18px Century Gothic';
    var maxWidth = 0;
    var metrics;
    for (var i=0; i < labels.length; ++i) {
        metrics = ctx.measureText(labels[i]);
        if (metrics.width > maxWidth) {
            maxWidth = metrics.width;
        }
    }
    return maxWidth;
}

function calcMaxXYEnclosure(nd) {
    var maxX = 0;
    var maxY = 0;
    function calcMaxXY(nd) {
        if (!nd.nodeType) {
            for (var i=0; i < nd.children.length; ++i) {
                if (nd.children[i].x > maxX) {
                    maxX = nd.children[i].x;
                }
                if (nd.children[i].y > maxY) {
                    maxY = nd.children[i].y;
                }
                calcMaxXY(nd.children[i]);
            }
        }
    }
    calcMaxXY(nd);
    return [maxX, maxY];
}

function calcCanvasSize(nd, rectWidth, rectHeight, canvas) {
    var maxXY = calcMaxXYEnclosure(nd);
    canvas.width = rectWidth * (maxXY[0] + 2)
    canvas.height = 4 * rectHeight * (maxXY[1] + 1)
}

function makeTable(Data, ItemNames, classNames) {
    var table = $('<table id="dataTable">');
    makeTopRow(ItemNames, table);
    for (var i=0; i < Data.length; ++i) {
        makeRow(i, Data[i], classNames, table);
    }
    $('#trainingData').append(table);
}

function makeTopRow(ItemNames, table) {
    var tr = $('<tr>');
    var td_list = [$('<td>').text('No.')];
    for (var i=0; i < ItemNames.length; ++i) {
        var td = $('<td>');
        var span = $('<span>');
        span.text(ItemNames[i]);
        span.on('dblclick', {labels: ItemNames, i: i}, labelEdit);
        span.attr('title', 'ダブルクリックで項目名を編集');
        td.append(span);
        td_list.push(td);
    }
    td_list.push($('<td>').text('Class'));
    for (var i=0; i < td_list.length; ++i) {
        tr.append(td_list[i]);
    }
    table.append(tr);
}

function makeRow(no, data, classNames, table) {
    var tr = $('<tr>');
    var td_no = $('<td>').text(String(no + 1));
    tr.append(td_no);

    for (var i=0; i < data.x.length; ++i) {
        var td_data = $('<td>');
        var td_span = $('<span>').attr('title', 'クリックでtrue/falseを切り替え');
        td_data.append(td_span);
        td_span.text(String(Boolean(data.x[i])));
        td_data.on('click', {i: i, x: data.x}, trHandler);
        td_data.css("background-color", (data.x[i]) ? trueColor:falseColor);
        tr.append(td_data);
    }

    var td_label = $('<td>');
    var select = $('<select>');
    makeOptions(data.y, classNames, select);
    select.on('change', {d: data}, selectHandler);
    td_label.append(select);
    tr.append(td_label);
    table.append(tr);
}

function addRow(event) {
    var no = event.data.D.length;
    var d = {x:[], y:0};
    for (var i=0; i < event.data.D[0].x.length; ++i) {
        d.x[i] = 0;
    }
    event.data.D.push(d);
    makeRow(no, d, event.data.classNames, event.data.table);
}

function delRow(event) {
    if (event.data.table.children().children().length <= 2) {
        return;
    }
    event.data.table.find('tr:last').remove();
    event.data.D.pop();
}

function addColumn(event) {
    $('tr').each(function (index) {
        if (index === 0) {
            event.data.L.push('default');
            var td_top = $('<td>');
            var span = $('<span>');
            span.text('default');
            span.on('dblclick', {labels: event.data.L, i: event.data.L.length - 1}, labelEdit);
            span.attr('title', 'ダブルクリックで項目名を編集');
            td_top.append(span);
            $(this).find('td:nth-last-child(2)').after(td_top);
        } else {
            event.data.D[index - 1].x.push(0);
            var d = event.data.D[index - 1];
            var td_data = $('<td>');
            var td_span = $('<span>').attr('title', 'クリックでtrue/falseを切り替え');
            td_data.append(td_span);
            td_span.text(String(Boolean(d.x[d.x.length - 1])));
            td_data.on('click', {i: d.x.length - 1, x: d.x}, trHandler);
            td_data.css("background-color", (d.x[d.x.length - 1]) ? trueColor:falseColor);
            $(this).find('td:nth-last-child(2)').after(td_data);
        }
    });
}

function delColumn(event) {
    $('tr').each(function (index) {
        if ($(this).children().length <= 3) {
            return;
        }
        if (index === 0) {
            event.data.L.pop();
        } else {
            event.data.D[index - 1].x.pop();
        }
        $(this).find('td:nth-last-child(2)').remove();
    });
}

function labelEdit(event) {
    if (!$(this).hasClass('on')) {
        $(this).addClass('on');
        var text = $(this).text();
        $(this).html('<input type="text" value="' + text + '" />');
        $(this).children('input').focus().blur(function () {
            var val = $(this).val();
            $(this).parent().removeClass('on').text(val);
            event.data.labels[event.data.i] = val;
        });
        $(this).children('input').keypress(function (e) {
            if (e.which === 13) {
                var val = $(this).val();
                $(this).parent().removeClass('on').text(val);
                event.data.labels[event.data.i] = val;
            }
        });
    }
}

function makeOptions(y, classNames, select) {
    select.children().remove();
    for (var i=0; i < classNames.length; ++i) {
        var htmlString = '<option value="' + i;
        if (y === i) {
            htmlString += '" selected>';
        } else {
            htmlString += '">';
        }
        select.append($(htmlString).text(classNames[i]));
    }
}

function updateOptionsHandler(event) {
    updateOptions(event.data.D, event.data.classNames);
}

function updateOptions(Data, classNames) {
    $('#classNames input').each(function (index) {
        classNames[index] = String($(this).val());
    });
    $('table select').each(function (index) {
        makeOptions(Data[index].y, classNames, $(this));
    });
}

function makeClassNameInput(Data, classNames) {
    var div = $('#classNames');
    div.children().remove();
    for (var i=0; i < classNames.length; ++i) {
        var input = $('<input value="' + classNames[i] + '">');
        input.on('blur', {classNames: classNames, D: Data}, updateOptionsHandler);
        div.append(input);
    }
    div.append($('<br/>'));
    var btnPls = $('<button id="pls">クラスの追加</button>');
    btnPls.on('click', {D:Data, classNames: classNames}, inputAddHandler);
    var btnMns = $('<button id="mns">クラスの削除</button>');
    btnMns.on('click', {D:Data, classNames: classNames}, inputRemoveHandler);
    div.append(btnPls);
    div.append(btnMns);
    div.append($('<br/>'));
}

function inputRemoveHandler(event) {
    var inputs = $('#classNames input');
    if (inputs.length > 1) {
        inputs.eq(-1).remove();
        event.data.classNames.pop();
        updateOptions(event.data.D, event.data.classNames);
    }
}

function inputAddHandler(event) {
    var defaultName = 'defaultClass';
    event.data.classNames.push(defaultName);
    var input = $('<input value="' + defaultName + '">');
    input.on('blur', {classNames: event.data.classNames, D: event.data.D}, updateOptionsHandler);
    $('#classNames input:eq(-1)').after(input);
    updateOptions(event.data.D, event.data.classNames);
}
