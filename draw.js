function drawTree(dn, labels, classNames, canvas, rectWidth, rectHeight) {
    var line_dx = rectWidth;
    var line_dy = rectHeight * 2;
    var text;

    if (dn.nodeType) {
        text = classNames[dn.node.getMaxProportionClass()];
    } else {
        text = labels[dn.node.theta];
    }

    if (!(dn.nodeType)) {
        var base_c = calcNodeBasePosition(dn.x, dn.y, line_dx, line_dy, rectWidth, rectHeight);

        var ln = dn.children[0];
        var left_c = calcNodePosition(ln.x, ln.y, line_dx, line_dy, rectWidth, rectHeight);
        var rn = dn.children[1];
        var right_c = calcNodePosition(rn.x, rn.y, line_dx, line_dy, rectWidth, rectHeight);

        drawConnection(base_c, left_c, 'false', canvas);
        drawConnection(base_c, right_c, 'true', canvas);

        drawTree(ln, labels, classNames, canvas, rectWidth, rectHeight);
        drawTree(rn, labels, classNames, canvas, rectWidth, rectHeight);
    }

    var node_c = calcNodePosition(dn.x, dn.y, line_dx, line_dy, rectWidth, rectHeight);
    drawNode(node_c, dn, text, canvas, rectWidth, rectHeight);
}

function calcNodePosition(x, y, line_dx, line_dy, nodeWidth, nodeHeight) {
    return [line_dx * x + nodeWidth, (line_dy + nodeHeight) * y + nodeHeight / 2];
}

function calcNodeBasePosition(x, y, line_dx, line_dy, nodeWidth, nodeHeight) {
    return [line_dx * x + nodeWidth, (line_dy + nodeHeight) * y + nodeHeight * 3 / 2];
}

function drawNode(c, node, label, canvas, rectWidth, rectHeight) {
    var ctx = canvas.getContext('2d');
    ctx.strokeStyle = 'rgb(0, 0, 0)';
    if (node.nodeType) {
        ctx.strokeRect(c[0] - rectWidth / 2, c[1], rectWidth, rectHeight);
    }

    ctx.font = "bold 20px 'MS Pゴシック'";
    ctx.textAlign = 'center';
    ctx.textBaseline = 'hanging';
    ctx.fillStyle = 'rgb(0, 0, 0)';
    ctx.fillText(label, c[0], c[1], rectWidth);
    var impurenessText = '不純度: ' + numToString(node.node.impureness);
    var metrics = ctx.measureText(impurenessText);
    //ctx.fillText('不純度: ' + numToString(node.node.impureness), c[0], c[1] + rectHeight / 2, rectWidth);
    ctx.fillText(impurenessText, c[0], c[1] + rectHeight / 2, metrics.width);
}

function drawConnection(c1, c2, text, canvas) {
    var rectWidth = 50;
    var rectHeight = 20;
    var ctx = canvas.getContext('2d');

    ctx.beginPath();
    ctx.strokeStyle = 'rgb(0, 0, 0)';
    ctx.moveTo(c1[0], c1[1]);
    ctx.lineTo(c2[0], c2[1]);
    ctx.stroke();

    var cc = [(c1[0] + c2[0]) / 2, (c1[1] + c2[1]) / 2];
    ctx.fillStyle = 'rgb(255, 255, 255)';
    ctx.fillRect(cc[0] - rectWidth / 2, cc[1] - rectHeight / 2, rectWidth, rectHeight);

    ctx.font = '18px Century Gothic';
    ctx.textAlign = 'center';
    ctx.textBaseline = 'middle';
    ctx.fillStyle = 'rgb(0, 0, 0)';
    ctx.fillText(text, cc[0], cc[1]);
}

var NodeDrawable = (function () {
    var NodeDrawable = function (node, depth) {
        this.x = -1;
        this.y = depth;
        this.node = node;
        this.mod = 0;
        this.initialize();
    }

    var pt = NodeDrawable.prototype;

    pt.initialize = function () {
        this.children = [];
        if (this.node.children.length !== 0) {
            this.nodeType = false;  //ノードタイプ(true: 葉ノード, false: 枝ノード)
            for (var i=0; i < this.node.children.length; ++i) {
                var child = new NodeDrawable(this.node.children[i], this.y + 1);
                this.children.push(child);
            }
        } else {
            this.nodeType = true;   //ノードタイプ(true: 葉ノード, false: 枝ノード)
        }
    }

    pt.calcLayout = function () {
        this.setup(0, [], []);
        this.addmods(0);
    }

    pt.setup = function (depth, nexts, offset) {
        if (nexts[depth] === undefined) {
            nexts[depth] = 0;
        }
        if (offset[depth] === undefined) {
            offset[depth] = 0;
        }
        for (var i=0; i < this.children.length; ++i) {
            this.children[i].setup(depth + 1, nexts, offset);
        }

        //this.node.y = depth;

        var place;

        if (this.nodeType) {
            place = nexts[depth];
            this.x = place;
            nexts[depth] += 2;
        } else {
            place = 0;
            for (var i=0; i < this.children.length; ++i) {
                place += this.children[i].x;
            }
            place /= this.children.length;
            offset[depth] = Math.max(offset[depth], nexts[depth] - place);
            place += offset[depth];
            nexts[depth] = place + 2;
        }

        this.x = place;

        this.mod = offset[depth];
    }

    pt.addmods = function (mod) {
        this.x += mod;

        for (var i=0; i < this.children.length; ++i) {
            this.children[i].addmods(mod + this.mod);
        }
    }

    return NodeDrawable;
})();

function clearCanvas(canvas) {
    var ctx = canvas.getContext('2d');
    ctx.clearRect(0, 0, canvas.width, canvas.height);
}

function numToString(n) {
    var n_str;
    if (Math.abs(n) >= 1 / 100) {
        n_str = n.toFixed(3);
    } else if (n === 0) {
        n_str = '0';
    } else {
        n_str = n.toExponential(3);
    }

    return n_str;
}
